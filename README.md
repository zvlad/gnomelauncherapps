# README #

## How to create a new launcher
1. in `/usr/bin` create a link of executable file you want to launch 
2. create file `<app_name>.desktop` like this:
```
[Desktop Entry]
    Name=name-to-display-under-icon(probably, it should be the same as <app_name> t. e. fileName)
    Exec=name-of-link-in-usr-bin-folder
    Icon=path/to/icon
    Type=Application
    Categories=Utility;
```
Then

1. put this file in `~/.local/share/applications` or to it subfolders
2. do re-login

    [1/11/2016 3:48 AM, source: (https://developer.gnome.org/integration-guide/stable/desktop-files.html.en)]